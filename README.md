## 寻开发小伙伴
我突发奇想做这个项目，是因为最近开始健身，才发现原来里面有很多学问，跟一同健身的小伙伴学习了不少知识。
我发现，很多健身者都会遇到一些小烦恼，特别是对热量管理的烦恼。
因为无论你是想增肌，变成各大肌霸；还是想减肥，瘦出马甲线；都要对热量做好控制，否则目标遥遥无期～

而，影响热量的东西很多，比如你中午吃了多少米饭？今天的番茄炒蛋油不油？今天卧推多做了一组？昨天睡觉没睡好……
都会影响热量的计算！

所以，我打算做一款APP，把这些变量都方便的管理起来，你只需要使用这个APP，记录下你吃了什么、做了什么，剩下的就交给这个APP，它会帮你管理好热量，并给出合理的调整建议！

但是，设计开发这样一款程序，工作量实在不小，而本人多年疏于练习，编码手艺也不大行了，所以诚邀对健身感兴趣的程序猿或程序媛朋友加入，我们一起为健身大家庭谋福利！

感兴趣者可以直接给我小纸条！

## 我们的目标
无论你的健身目标是什么，增肌也好，减肥也罢，让我们帮助你更快达成目标！

祝你身体健康！

## 项目信息
本项目包含两个子项目，分别为安卓手机APP和苹果手机APP：<br/>
安卓：https://gitlab.com/litian/agym <br/>
苹果：https://gitlab.com/litian/igym

## 项目效果
首页<br/>
![](images/homepage.png)
